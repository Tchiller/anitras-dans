\version "2.16.0"

violeI =  \relative c'' {
	\time 3/4
	\key c\major
        \tempo "Tempo di Mazurka" 4 = 160

	b2.\pp\fermata ~
	b4 r\fermata r
	r^"pizz." e,,\p e
	r e e
	r e e
	r e e 
% volta (1)
 \repeat volta 2 {
      \grace { s16 s}
	r4 e e 
	r e e
	r e e

	r e a
	r a\cresc g
	r fis fis
	r a\dim g
	r fis fis
	b r a'
	gis\cr r g\!
	fis\decr r f\!
	e r dis
% 2
	e,8 dis\cr e fis g a\!
	b\decr e b a\! g fis
	e\cr fis g a b4->\f\!
 }

	\alternative {
	{ e-> r r } { e-> r r }
	}

	\repeat volta 2 {
	r4 d\p e
	r d\cr e\!
	r d e
	r d\decr e\!
	r8 f[ d c] b r

	r4 r8 d b a 
	gis r r4 r8 b
	gis[ f] e r r4
	r g' a
	r g\cr a\!
	r g a
	r g\decr a\!
	r8 bes[ g f] e r
	r4 r8 g e d
% 3
	cis8 r r4 r8 e
	cis[ bes] a r r4 
	r a\mp a
	r a a
	r a a
	r a a
	r a a
	r a a
	r a a

	r a\pp a
	r a a
	r a a
	r a a
	r a a
	r a a
	r a a
	r a a
	r r r8 c8\upbow^"arco"
% 4
\acciaccatura { f16[ g] }
  f8 e f g a bes
	c8( f  c2)
	r2.
	r4 r r8 e,\cresc
\acciaccatura { a16[ b] }
  a8 gis a b c d
	e8( a  e2)
	r2.
	r4 r r8 b,\upbow\ff

\acciaccatura { fis'16[ g] }
  fis8 eis\cr fis g a b\!
	c8\decr ( c,\!  fis2)
	r2.
	r4 r r8	e,\dim
\acciaccatura { b'16^"poco rit."[ c] }
  b8 ais b\cr c d e\!
	f\decr( f,  b2)\!
	r4^"a tempo"_"pizz." e,\p e
	r e e
	r e e
% 5
	r e e
	r a->\cresc g
	r fis fis
	r a\dim g
	r fis fis
	r d'->\cresc c
	r b b
	r d\dim c
	r b b
	e r d'

	cis r\cr c\!
	b\decr r\! ais
	a r gis
	a,8 gis a\cr b c d\!
	e\decr a e d c\! b
	a\cr b c d\! e4\f-> }
\alternative {
{a4-> r r} 
{a4-> r r} 
}
r2. 
e2.\pp\fermata ~ 
e4 r r \bar "|."
}



violeII =  \relative c' {
	\time 3/4
	\key c\major
        \tempo "Tempo di Mazurka" 4 = 160

	e2.\pp\fermata(
	 e4) r\fermata r
	r^"pizz." e,\p e % original r e,^""pizz." e -- Changed for layout (P. Tönnies)
	r e e
	r e e
	r e e 
% volta (1)
 \repeat volta 2 {
      \grace { s16 s}
	r4 e e
	r e e
	r e e

	r e a
	r a\cresc g
	r fis fis
	r a\dim g
	r fis fis
	b r f'
	e\cr r dis\!
	d\decr r \! cis
	c r b
% 2
	e,8 dis e\cr fis g a\!
	b\decr e b a\! g fis
	e\cr fis g a\! b4->\f
 }
	
	\alternative {
	{ e-> r r } { e-> r r }
	}

	\repeat volta 2 {
	r4 gis,\p d'
	r gis,\cr d'\!
	r gis, d'
	r gis,\decr d'\!
	r8 f[ d c] b r

	r4 r8 d b a 
	gis r r4 r8 b
	gis[ f] e r r4
	r c' g'
	r cis,\cr g'\!
	r cis, g'
	r cis,\decr g'\!
	r8 bes[ g f] e r
	r4 r8 g e d
% 3
	cis8 r r4 r8 e
	cis[ bes] a r r4 
	r a\mp a
	r a a
	r a a
	r a a
	r a a
	r a a
	r a a

	r a\pp a
	r a a
	r a a
	r a a
	r a a
	r a a
	r a a
	r a a
	r a a
% 4
	r a a
	r a a
	r c\cresc c
	r c c
	r c_"più cresc." c
	r c c
	r e\ff dis
	r e dis

	r e dis
	r e dis
	r a\dim gis
	r a gis
	r^"poco rit." a gis
	r a gis
	r^"a tempo" e\p e
	r e e
	r e e 
% 5
	r e e
	r a->\cresc g
	r fis fis
	r a\dim g
	r fis fis
	r d'->\cresc c
	r b b
	r d\dim c
	r b b
	e r bes'

	a r\cr gis\!
	g\decr r\! fis
	f r e
	a,8 gis\cr a b c d\!
	e\decr a e d\! c b
	a\cr b c d\! e4->\f }
\alternative {
{a4-> r r } 
{a4-> r r}
}
r2. 
a,2.\pp\fermata ~ 
a4 r r 
}
