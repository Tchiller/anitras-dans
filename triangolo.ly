\version "2.16.0"

triangolo = \drummode {
	\time 3/4
        \tempo "Tempo di Mazurka" 4 = 160

	tri2.:16\p\fermata
	tri4 r\fermata r
        \compressFullBarRests
	R2.*4|

% volta (1)
 \repeat volta 2 {
	tri4\p r r
R2.|
	tri4 r r
	
R2.|
	tri4 r r
R2.|
	tri4 r r
R2.|
	tri4 r r
	tri4 r r
	tri4 r r
	tri4 r r
% 2
	tri r r
R2.|
R2.|
 }

	\alternative {
	{R2.| } {R2.|}
	}

 \repeat volta 2 {
	tri4 r r
R2.|
	tri4 r r
R2.|
        
	R2.*4|\mark \default
	tri4 r r
R2.|
	tri4 r r
R2.|
	R2.*4|
% 3
	R2.*3|
	tri4 r r
R2.|
	tri4 r r
R2.|

	R2.*3|
	tri4 r r
R2.|
	tri4 r r
R2.|\mark \default
	R2.*8|
	R2.*6|
                R2.^"poco rit."|
        R2.|
%        <<
%        { R2.^"poco rit."| 
%          R2. \cueClefUnset}
%        \new CueVoice {
%        \cueClef "alto" \stemUp b8_"Viole" ais b c d e|
%	f( f,  b2)|
%        } % Todo: check how to obtain real staff for Viola
%        >>
	tri4\p^"a tempo" r r
R2.|
	tri4 r r 
% 5
R2.|
	tri4 r r
R2.|
	tri4 r r
R2.|
	tri4 r r
R2.|
	tri4 r r 
R2.|\mark \default
	tri4 r r

	tri r r
	tri r r
	tri r r 
	tri r r
R2.|
R2.|
 }
\alternative {
{R2.|} 
{R2.|} 
}
R2.|
tri2.:16\fermata 
tri4 r r \bar "|."
}