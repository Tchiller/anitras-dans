I am recently re-typesetting Anitras Dans from Edvard Grieg. The source of this project was taken from http://www.mutopiaproject.org/cgibin/piece-info.cgi?id=281 and will be uploaded to there again.
The project is done by http://lilypond.org/ with version 2.16.0

TODO:

- Combine Viola staffs in score and parts but separate in bars 55 - 70 (two staffs)
- Combine Cello staffs in score and parts for bars 29 - 39
- Delete empty staff lines for Triangolo in score (bars 53 - 69)
- vertically adjust fermatas for high notes
- vertically align all hairpins (with dynamic marks)
- replace "a2" by "unisono" (or "div." and "unis.")
- bar 68: set dotted lines between "poco rit." and "a tempo"
- check source, if triangolo in bar 1 and 91 should have three beams (tremolo)
- Triangolo part - bar 68: write two bars of viola notes for help
- vertically adjust fermatas where possible
- finally adjust all "cresc" "divisi" etc. horizontally for end of lines and bar-lines
