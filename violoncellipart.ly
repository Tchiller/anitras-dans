\version "2.16.0"
\include "header.ly"

\paper {
  indent = 3.0\cm  % space for instrumentName
  short-indent = 1.5\cm  % space for shortInstrumentName
}

violoncelliI =  \relative c {
   \override DynamicTextSpanner #'style = #'none
	\time 3/4
	\key c\major
        \tempo "Tempo di Mazurka" 4 = 160

	r2.\fermata
	r4 r\fermata r
	a\p^"pizz." r e'8 e
	a,4 r e'8 e
	a,4 r e'8 e
	a,4 r e'8 e 
% volta (1)
 \repeat volta 2 {
	a,4 r e'8 e
	a,4 r e'8 e
	a,4 r e'8 e

	a,4 r e'8 e
	a,4\cresc r e'8 e
	a,4 r e'8 e
	a,4\dim r e'8 e
	b4 r r
	 b'8[-.^"arco"\pp ais-. fis-. b-. a-. f-.]
	 b[-.\cr gis-. e-. b'-. g-. dis-.\!]
	 b'[-.\decr fis-. d-. b'-. f-. des-.\!]
	 b'[-. e,-. c-. b'-. dis,-. b-.]
% 2

\acciaccatura { e16[ fis] }
  e4-. r r
	r2.
	r4 r4 b'4->\f^"pizz." 
 }

	\alternative {
	{ e-> r r } { e-> r r }
	}

	\repeat volta 2 {
	r4 e,\p gis
	r e\cr gis\!
	r e gis
	r e\decr gis\!
	r r r8 f'8\downbow->_"arco"(

	 d8)-.[ c-.(]  b) r r4
	r8 d8-.\downbow([  b)-. a-.(]  gis) r
	r4 e8^"pizz." d c b
	a4 a' cis
	r a\cr cis\!
	r a cis
	r a\decr cis\!
	r r r8 bes'->\downbow_"arco" ( 
	 g)-.[ f-.(]  e)-. r r4
% 3
	r8 g-.->\downbow([  e)-. d-.(]  cis) r
	r4 a8^"pizz."\cr g f e\!
	d4\mp r a8 a
	d4 r a8 a
	d4 r a8 a
	d4 r r
	d r r
	d r r
	d r r

	d\pp r a8 a
	d4 r a8 a
	d4 r a8 a
	d4 r r
	d r r
	d r r
	d r r
	ees2.^"arco" ~
	ees ~
% 4
	ees ~
	ees
	fis\cresc ~
	fis ~
	fis_"più cresc." ~
	fis 
	c'2\ff\decr(  b4)\!
	c2\decr(  b4)\!

	c2\decr(  b4)\!
	c2\decr(  b4)\!
	f2\decr (  e4)\!
	f2\decr(  e4)\!
	f2\decr^"poco rit."(  e4)\!
	f2\decr(  e4)\!
	r^"a tempo" r e\p
        
\acciaccatura { a16[ b] }
  a8 gis\cr a b c d\!
	e\decr ( a e4  d)\!
% 5
	c8( e  c2)
	f2->(  e4)
	dis2.\cresc
	a'2->\dim(  g4)
	fis2 r4
	bes,2->(  a4)
	gis2.\cresc
	d'2->\dim(  c4)
	b2 r4
	e8-.\pp dis-. b-. e-. d-. bes-.

	e-. cis-. a-.\cr e'-. c-. gis-.\!
	e'-.\decr b-. g-. e'-.\! ais,-. fis-.
	e'-. a,-. f-. e'-. gis,-. e-.
        
\acciaccatura { a16[ b] }
  a4-. r r
	r2.
	r4 r e'4->\f^"pizz." }
\alternative {
{a4-> r r} 
{a4-> r e,\pp} 
}
a, r r 
r2.\fermata 
r \bar "|."
}



violoncelliII =  \relative c {
   \override DynamicTextSpanner #'style = #'none
	\time 3/4
	\key c\major

	r2.\fermata
	r4 r\fermata r
	a\p^"pizz." r r
	a r r
	a r r
	a r r 
% volta (1)
 \repeat volta 2 {
	a4 r r
	a r r
	a r r

	a r r
	a\cresc r r
	a r r
	a\dim r r
	b r r
	b r b'
	b,\cr r b'\!
	b,\decr r b'\!
	b, r b'
% 2
	e,4 r r
	r2.
	r4 r4 b'4\f-> 
 }

	\alternative {
	{ e-> r r } { e-> r r }
	}

\repeat volta 2 {
	e,,4\p r b'8 b
	e,4\cr r b'8 b\!
	e,4 r b'8 b
	e,4\decr r b'8 b\!
	e,4 r r8 f''8\downbow->_"arco"(

	 d8)-.[ c-.(]  b) r r4
	r8 d8-.\downbow([  b)-. a-.(]  gis) r
	r4 e8^"pizz." d c b
	a4 a' cis
	r a\cr cis\!
	r a cis
	r a\decr cis\!
	r r r8 bes'->\downbow_"arco"( 
	g-.)[ f-.(]  e-.) r r4
% 3
	r8 g->\downbow([  e) d(]  cis) r
	r4 a8^"pizz."\cr g f e\!
	d4\mp r r
	d r r
	d r r
	d r r
	d r r
	d r r
	d r r

	d\pp r r
	d r r
	d r r
	d r r
	d r r
	d r r
	d r r
	ees r r
	ees r r
% 4
	ees r r
	ees r r
	fis\cresc r r
	fis r r 
	fis_"più cresc." r r 
	fis r r 
	b,\ff r fis'8 fis
	b,4 r fis'8 fis

	b,4 r fis'8 fis
	b,4 r b8 b
	e,4\dim r b'8 b
	e,4 r b'8 b
	e,4^"poco rit." r e'8\p e
	e,4 r e'8\p e
	a,4^"a tempo" r e'8 e
	a,4 r e'8 e
	a,4 r e'8 e
% 5
	a,4 r e'8 e
	a,4\cresc r r 
	a r r
	a\dim r r
	b r r
	d\cresc r r
	d r r
	d\dim r r
	e r r
	e r e'

	e, r\cr e'\!
	e,\decr r\! e'
	e, r e'
	a, r r
	r2.
	r4 r e\f-> }
\alternative {
{a4-> r r} 
{a4-> r e\pp}
}
a, r r
r2.\fermata 
r
}

\score {
\new StaffGroup<<
   \new Staff \with {
	midiInstrument = #"pizzicato strings"
    instrumentName = "Violoncelli I"
	shortInstrumentName = #"Vc. I"
	\clef "bass"
   }
	\violoncelliI

   \new Staff \with {
	midiInstrument = #"pizzicato strings"
	instrumentName = "Violoncelli II"
	shortInstrumentName = #"Vc. II"
	\clef "bass"
   }
	\violoncelliII

>>
\layout {}

  \midi {
    \tempo 4 = 160
    }
}