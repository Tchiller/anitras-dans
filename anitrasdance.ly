\version "2.16.0"

#(set-global-staff-size 16)
\paper {
  indent = 3\cm  % space for instrumentName
  short-indent = 1.5\cm  % space for shortInstrumentName
}

\include "header.ly"
\include "violiniI.ly"
\include "violiniII.ly"
\include "viole.ly"
\include "violoncelli.ly"
\include "contrabassi.ly"
\include "triangolo.ly"

\score {
  <<
\new StaffGroup <<
   \new Staff \with {
      \override DynamicTextSpanner #'style = #'none
	midiInstrument = #"violin"
	instrumentName = \markup {
          \column { "Violini I"
                    \line { "(con sordini)" }
          }
        }
	shortInstrumentName = #"Vl. I"
        soloText = #"I."
        soloIIText = #"II."
   }
	\partcombine
		 \violinioneI
		 \violinioneII

   \new Staff \with {
      \override DynamicTextSpanner #'style = #'none
	midiInstrument = #"pizzicato strings"
	instrumentName = \markup {
          \column { "Violini II"
                    \line { "(con sordini)" }
          }
        }
	shortInstrumentName = #"Vl. II"
        soloText = #"I."
        soloIIText = #"II."
   }
        \partcombine
		\violinitwoI
		\violinitwoII

    \new GrandStaff <<
   \new Staff \with {
      \override DynamicTextSpanner #'style = #'none
	midiInstrument = #"pizzicato strings"
	instrumentName = #"Viole I"
	shortInstrumentName = #"Vla. I"
	\clef "alto"
   }
	\violeI

   \new Staff \with {
      \override DynamicTextSpanner #'style = #'none
	midiInstrument = #"pizzicato strings"
	instrumentName = #"Viole II"
	shortInstrumentName = #"Vla. II"
	\clef "alto"
   }
	\violeII
    >>

      \new GrandStaff <<
   \new Staff \with {
      \override DynamicTextSpanner #'style = #'none
	midiInstrument = #"pizzicato strings"
	instrumentName = #"Violoncelli II"
	shortInstrumentName = #"Vc. I"
	\clef "bass"
   }
	\violoncelliI

   \new Staff \with {
      \override DynamicTextSpanner #'style = #'none
	midiInstrument = #"pizzicato strings"
	instrumentName = #"Violoncelli II"
	shortInstrumentName = #"Vc. II"
	\clef "bass"
   }
	\violoncelliII
      >>

   \new Staff \with {
      \override DynamicTextSpanner #'style = #'none
	midiInstrument = #"pizzicato strings"
	instrumentName = #"Contrabassi"
	shortInstrumentName = #"Cb."
	transposing = #-12
	\clef "bass"
   }
	\contrabassi
        >>

   \new DrumStaff \with {
      \override DynamicTextSpanner #'style = #'none
	midiInstrument = #"tinkle bell"
	instrumentName = #"Triangolo"
	shortInstrumentName = #"Tr."
         \override StaffSymbol #'line-count = #1
   \override BarLine #'bar-extent = #'(-1.5 . 1.5)
   }
       \triangolo

  >>

\layout {
  \context {
    \Staff \RemoveEmptyStaves
    % To use the setting globally, uncomment the following line:
    \override VerticalAxisGroup #'remove-first = ##t
  }
}

  \midi {
    \tempo 4 = 160
    }

}