\version "2.16.0"
\include "header.ly"

\paper {
  indent = 3.0\cm  % space for instrumentName
}

contrabassi =  \relative c {
   \override DynamicTextSpanner #'style = #'none
	\time 3/4
	\key c\major
        \tempo "Tempo di Mazurka" 4 = 160
                \compressFullBarRests

	r2.^\fermata
	r4 r^\fermata r
	a\p^"pizz." r r
	a r r
	a r r
	a r r 
% volta (1)
 \repeat volta 2 {
	a4 r r
	a r r
	a r r

	a r r
	a\cresc r r
	a r r
	a\dim r r
	b r r
	b r r
	b\cr r r\!
	b\decr r r\!
	b r r
% 2
	e4 r r
	r2.
	r4 r4 b'4->\f 
 }

	\alternative {
	{ e-> r r } { e-> r r }
	}

 \repeat volta 2 {
	e,4\p r r 
	e\cr r r\!
	e r r
	e\decr r r\!
	e r r

	r2.
R2.*2|
	a4 r r
	a\cr r r\!
	a r r
	a\decr r r\!
	a r r
	r2.
% 3
	r2.
	r
	d,4\mp r r
	r2.
	r
	d4 r r
R2.*3|

	d4\pp r r
	r2.
	r
	d4 r r 
R2.*3|
	ees4 r r
	ees r r
% 4
	ees r r
	ees r r 
	fis\cresc r r
	fis r r
	fis_"più cresc." r r
	fis r r
	b,\ff r r
	b r r

	b r r 
	b r r 
	e,\dim r r 
	e r r
	e^"poco rit." r r
	e r r
	a\p^"a tempo" r r 
	a r r
	a r r
% 5
	a r r
	a\cresc r r
	a r r
	a\dim r r
	b r r
	d\cresc r r
	d r r
	d\dim r r
	e r r
	e r r

	e\cr r r\!
	e\decr r r\!
	e r r
	a r r
	r2. 
	r }
\alternative {
{r2.} 
{r4 r e\pp}
}
a, r r 
r2.\fermata 
r \bar "|."
}

\score {

   \new Staff \with {
	midiInstrument = #"pizzicato strings"
	instrumentName = #"Contrabassi"
	\clef "bass"
   }
	\contrabassi

\layout {}

  \midi {
    \tempo 4 = 160
    }
}