\version "2.16.0"
\include "header.ly"

\paper {
  indent = 3.0\cm  % space for instrumentName
}

violinitwoI =  \relative c''' {
   \override DynamicTextSpanner #'style = #'none
	\time 3/4
	\key c\major
        \tempo "Tempo di Mazurka" 4 = 160

	b2._\pp\fermata^"divisi" ~
	b4 r\fermata r
	r c,,\p^"pizz." d
	r e f
	r e d
	r c b 
% volta (1)
 \repeat volta 2 {
	r4 c d
	r e f
	r e d

	r c e
	r f\cresc e
	r dis dis
	r f\dim e
	r dis dis
	b' r b'
	b,\cr r b'\!
	b,\decr r b'\!
	b, r b'
% 2
	e,,8 dis\cr e fis g a\!
	b\decr e b a\! g fis
	e\cr fis g a b4->\f\! 
 }

	\alternative {
	{ e-> r r} 
    { e-> r r}
	}

	\repeat volta 2 {
	gis,2\p^"arco" ~ gis4-.
	gis2\cr ~ gis4-.\!
	gis2.\decr ~
	gis4\! r r
	r8 f'8_"pizz."[ d c] b r

	r4 r8 d b a 
	gis r r4 r8 b
	gis[ f] e r r4
	cis'2^"arco" ~ cis4-.
	cis2\cr ~ cis4-.\!
	cis2.\decr ~
	cis4\! r r 
	r8 bes'^"pizz."[ g f] e r
	r4 r8 g e d
% 3
	cis8 r r4 r8 e
	cis[ bes] a r r4
	r fis\mp^"divisi" g
	r a b
	r a g
	r fis e
	r fis g
	r a b
	r a g

	r f\pp g
	r a bes
	r a g
	r f e
	r f g
	r a bes
	r a g
	r f f
	r f f
% 4
	r f f
	r f f
	r a\cresc a
	r a a
	r a_"più cresc." a
	r a a
	r a\ff a
	r a a

	r a a
	r a a
	r d,\dim d
	r d d
	r^"poco rit." d d
	r d d
	r^"a tempo" c\p d
	r e f
	r e d
% 5
	r c c
	r f->\cresc e
	r dis dis
	r f\dim e
	r dis dis
	r bes'->\cresc a
	r g g
	r bes\dim a
	r gis gis
	e'4 r e'

	e,\cr r e'\!
	e,\decr r e'\!
	e, r e'
	a,,8 gis\cr a b c d\!
	e\decr a e d\! c b
	a\cr b c d\! e4\f-> }

\alternative {
{a4-> r r} 
{a4-> r r}
}
r2.
e2.\pp\fermata ~ 
e4 r r \bar "|."
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
violinitwoII =  \relative c'' {
	\time 3/4
	\key c\major
        \tempo "Tempo di Mazurka" 4 = 160

	e2._\pp\fermata ~
	e4 r\fermata r
	r a,,\p b
	r c d
	r c b
	r a gis 
% volta (1)
 \repeat volta 2 {
	r4 a b
	r c d
	r c b

	r a c
	r c\cresc c
	r c c
	r c\dim c
	r b b 
	b r b'
	b,\cr r b'\!
	b,\decr r b'\!
	b, r b'
% 2
	e,8 dis\cr e fis g a\!
	b\decr e b a\! g fis
	e\cr fis g a b4->\f\! 
 }

	\alternative {
	{ e-> r r} 
    { e-> r r}
	}

	\repeat volta 2 {
	gis,2\p^"arco" ~ gis4-.
	gis2\cr ~ gis4-.\!
	gis2.\decr ~
	gis4\! r r
	r8 f'8_"pizz."[ d c] b r

	r4 r8 d b a 
	gis r r4 r8 b
	gis[ f] e r r4
	cis'2^"arco" ~ cis4-.
	cis2\cr ~ cis4-.\!
	cis2.\decr ~
	cis4\! r r 
	r8 bes'^"pizz."[ g f] e r
	r4 r8 g e d
% 3
	cis8 r r4 r8 e
	cis[ bes] a r r4
	r d,\mp e
	r fis g
	r fis e
	r d cis
	r d e
	r fis g
	r fis e

	r d\pp e
	r f g
	r f e
	r d cis
	r d e
	r f g
	r f e
	r c c
	r c c
% 4
	r c c
	r c c
	r e\cresc e
	r e e
	r e e 
	r e e
	r fis\ff fis
	r fis fis

	r fis fis
	r fis fis
	r b,\dim b
	r b b
	r^"poco rit." b b
	r b b
	r^"a tempo" a\p b
	r c d
	r c b
% 5
	r a a
    r c->\cresc c
	r c c
	r c\dim c
	r b b
	r f'->\cresc f
	r f f
	r f\dim f
	r e e
	e4 r e'
	
	e,\cr r e'\!
	e,\decr r e'\!
	e, r e'
	a,8 gis\cr a b c d\!
	e\decr a e d\! c b
	a\cr b c d\! e4\f-> }

\alternative {
{a4-> r r} 
{a4-> r r} 
}  
r2. 
a,2. ~ 
a4 r r
}

\score {

   \new Staff \with {
	midiInstrument = #"violin"
	instrumentName = \markup {
          \column { "Violini II"
                    \line { "(con sordini)" }
          }
        }
   }
	\partcombine
		\violinitwoI
		\violinitwoII

\layout {}

  \midi {
    \tempo 4 = 160
    }
}