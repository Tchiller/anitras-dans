\version "2.16.0"

violinioneI =  \relative c''' {
	\time 3/4
	\key c\major
        \tempo "Tempo di Mazurka" 4 = 160

	e2._\pp-\markup { \right-align divisi}\fermata ~
	e4 r\fermata r
	r2.
	r
	r
	r4 r e,,\p \bar "||" \break
% volta (1)
\repeat volta 2 {
\acciaccatura { a16[ b] }
  a8-. gis-. a-. b-.( c-.  d)-.
	e8(  a) e4--( d--\trill)
	c8( a'  c,4) b--\trill\upbow( 
	a2) r4 
         
    a8--\cresc\(( c  a4) g--\trill\)
	fis8\((  c')  fis,2--\)
	f8\dim\(( c'  f,4--) e--\trill\)  
	dis8(  b') dis,2 
	b'8-.\pp ais-. fis-. b-. a-. f-. 
	b-.\cr gis-. e-. b'-. g-. dis-.\! 
	b'-.\decr fis-. d-. b'-. f-. des-.\! 
	b'-. e,-. c-. b'-. dis,-. b-.
% 2
\acciaccatura { e16[ fis] }
  e4-. r r 
	r2. 
	e8\cr^"pizz." fis g a\! b4->\f }
\alternative {
{ e4-> r e,^"arco"\p} { e'4-> r r }
}
 \repeat volta 2 {
	f4.\p^"divisi"^"arco"\( (  e8) e([  d)\)]
	d4.\( (  e8\cr) e([ f)\)\!]
	f2.\decr( 
	 e4)\! r r 
	r r r8 f8->\downbow( 

	 d)-.[ c-.(]  b)-. r r4
	r8 d-.\downbow([  b)-. a-.(]  gis-.) r
	r2.\mark \default
	bes'4._"divisi"\( (  a8) a([  g)\)]
	g4.\cr\( (  a8) a([  bes)\)\!]
	bes2.\decr(
	 a4)\! r r 
	r r r8 bes->\downbow( 
	 g)-.[ f-.(]  e)-. r r4 
% 3
	r8 g-.->\downbow([  e)-. d-.(]  cis)-. r 
	r2. 
	r 
	r 
	r4 r a\mp
\acciaccatura { d16[ e] }
  d8-. cis-. d-. e-.( fis-.  g-.)
	a\(( d  a4) g--\trill\)
	fis8( d'  fis,4) e--\trill(
	 d2) r4

	r2.
	r 
	r4 r a\pp 
\acciaccatura { d16[ e] }
  d8-. cis-. d-. e-.( f-.  g)-. 
	a\(( d  a4) g--\trill\) 
	f8( d'  f,4) e--\trill(
	 d2) r4 
	r r r8 c\upbow
\acciaccatura { f16[ g] }
  f8-. e-. f-. g-.( a-.  bes-.)
% 4
	c8( f  c2--)
	r2.
	r4 r r8 e,\cresc 
\acciaccatura { a16[ b] }
  a8-. gis-. a-. b-.( c-.  d)-. 
	e\((  a)  e2--\)
	r2.
	r4 r r8 b,\ff 
\acciaccatura { fis'16[ g] }
  fis8 eis\cr fis g a b\!

	c\decr\((  c,)  fis2--\)\!
	r2. 
	r4 r r8 e,8\dim
\acciaccatura { b'16[ c] }
 b8 ais b c d e
	f^"poco rit."\( (  f,)  b2--\)
	r4 r e4\p\upbow^"divisi"
\acciaccatura { a16^"a tempo"[ b] }
 a8-. gis-. a-. b-.( c-. d)-.
	e(  a) e4--( d--\trill)
	c8(  e) c4\upbow b--\trill(
% 5
	 a2) r4
	a8\downbow->\cresc( c a4-- g--\trill) 
	fis8\((  c')  fis,2--\)
	f8->\dim\( (  c') f,4--( e--\trill)\)
	dis8\( (  b')  dis,2\)--
	d8->\cresc\( (  f) d4--( c--\trill)\)
	b8\((  f')  b,2\)--
	bes8->\dim\( (  f') bes,4--( a--\trill)\)
	gis8\((  e')  gis,2\)--
	e'8-.\pp dis-. b-. e-. d-. bes-.

	e-. cis-. a-. e'-.\cr c-. gis-.\!
	e'-.\decr b-. g-. e'-.\! ais,-. fis-.
	e'-. a,-. f-. e'-. gis,-. e-.
\acciaccatura { a16[ b] }
 a4-. r r
	r2.
	a8\cr^"pizz." b c d\! e4\f-> } 
\alternative {
{a4-> r r} 
{a4-> r r} 
} 
r2. 
a2.\pp\fermata-\markup { \right-align divisi} ~ 
a4 r r \bar "|."
}

violinioneII =  \relative c''' {
	\time 3/4
	\key c\major
        \tempo "Tempo di Mazurka" 4 = 160
        
    gis2._\pp\fermata ~
	gis4 r\fermata r
	r2.
	r
	r
	r4 r e,\p \bar "||" \break
% volta (1)
\repeat volta 2 {
\acciaccatura { a16[ b] }
  a8-. gis-. a-. b-.( c-.  d)-.
	e8(  a) e4--( d--\trill)
	c8( a'  c,4) b--\trill\upbow( 
	a2) r4 
         
    a8--\cresc\(( c  a4) g--\trill\)
	fis8\((  c')  fis,2--\)
	f8\dim\(( c'  f,4--) e--\trill\)  
	dis8(  b') dis,2 
	b'8-.\pp ais-. fis-. b-. a-. f-. 
	b-.\cr gis-. e-. b'-. g-. dis-.\! 
	b'-.\decr fis-. d-. b'-. f-. des-.\! 
	b'-. e,-. c-. b'-. dis,-. b-.
% 2
\acciaccatura { e16[ fis] }
  e4-. r r 
	r2. 
	e8\cr^"pizz." fis g a\! b4->\f }
\alternative {
{ e4-> r e,^"arco"\p} 
{ e'4-> r r}
}
\repeat volta 2 {
	d4.\p\( (  c8) c([ b)\)]
	b4.\(( c8)\cr c([ d)\)\!]
	d2.\decr ~ 
	d4\! r r 
	r r r8 f8->\downbow( 

	 d)-.[ c-.(]  b)-. r r4
	r8 d-.\downbow([  b)-. a-.(]  gis-.) r
	r2.
	g'4.\( (  f8) f([ e)\)]
	e4.\cr\( (  f8) f([ g)\)\!]
	g2.\decr(
	g4)\! r r 
	r r r8 bes->\downbow( 
	 g)-.[ f-.(]  e)-. r r4 
% 3
	r8 g-.->\downbow([  e)-. d-.(]  cis)-. r 
	r2. 
	r 
	r 
	r4 r a\mp
\acciaccatura { d16[ e] }
  d8-. cis-. d-. e-.( fis-.  g-.)
	a\(( d  a4) g--\trill\)
	fis8( d'  fis,4) e--\trill( 
	 d2) r4

	r2.
	r 
	r4 r a\pp 
\acciaccatura { d16[ e] }
  d8-. cis-. d-. e-.( f-.  g)-. 
	a\(( d  a4) g--\trill\) 
	f8( d'  f,4) e--\trill(
	 d2) r4 \mark \default
	r r r8 c\upbow
\acciaccatura { f16[ g] }
  f8-. e-. f-. g-.( a-.  bes-.)
% 4
	c8( f  c2--)
	r2.
	r4 r r8 e,\cresc 
\acciaccatura { a16[ b] }
  a8-. gis-. a-. b-.( c-.  d)-. 
	e\((  a)  e2--\)
	r2.
	r4 r r8 b,\ff 
\acciaccatura { fis'16[ g] }
  fis8 eis\cr fis g a b\!

	c\decr\((  c,)  fis2--\)\!
	r2. 
	r4 r r8 e,8\dim
\acciaccatura { b'16[ c] }
 b8 ais b c d e
	f^"poco rit."\( (  f,)  b2--\)
	r4 r e,4\p\upbow
\acciaccatura { a16[ b] }
 a8-. gis-. a-. b-.( c-. d)-.
	e(  a) e4--( d--\trill)
	c8(  e) c4\upbow b--\trill(
% 5
	 a2) r4
	a'8\downbow->\cresc( c a4-- g--\trill) 
	fis8\((  c')  fis,2--\)
	f8->\dim\( (  c') f,4--( e--\trill)\)
	dis8\( (  b')  dis,2--\)
	d8->\cresc\( (  f) d4--( c--\trill)\)
	b8\((  f')  b,2\)--
	bes8->\dim\( (  f') bes,4--( a--\trill)\)
	gis8\((  e')  gis,2\)--\mark \default
	e'8-.\pp dis-. b-. e-. d-. bes-.

	e-. cis-. a-. e'-.\cr c-. gis-.\!
	e'-.\decr b-. g-. e'-.\! ais,-. fis-.
	e'-. a,-. f-. e'-. gis,-. e-.
\acciaccatura { a16[ b] }
 a4-. r r
	r2.
	a8\cr^"pizz." b c d\! e4\f-> } 
\alternative {
{a4-> r r} 
{a4-> r r} 
}
r2. 
c,2.\pp\fermata ~ 
c4 r r \bar "|."
}
